<?php


/**
 * @package  jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 * @doc https://docs.moodle.org/dev/String_API
 */
 
defined('MOODLE_INTERNAL') || die();
 
$string['plugintitle'] = 'This is a complete template example of a local plugin';