<?php

/**
 * @package  jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 * @doc https://moodle.org/mod/forum/discuss.php?d=262403
 */

// Details of classes that have been renamed to fit in with autoloading.

defined('MOODLE_INTERNAL') || die();

