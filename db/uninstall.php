<?php

/**
 * @package  jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 * @doc 
 */

// Allows you to execute a PHP code before the plugin's database tables and data are dropped during the plugin uninstallation.

defined('MOODLE_INTERNAL') || die();

function xmldb_local_jsdemo_uninstall(){
    // Uninstallation code goes here
}