<?php

/**
 * @package  jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 * @doc https://docs.moodle.org/dev/Adding_a_web_service_to_a_plugin
 *      https://docs.moodle.org/dev/Web_services_API
 *      https://docs.moodle.org/dev/External_functions_API
 */

// External functions and web services provided by your plugin are described here.

defined('MOODLE_INTERNAL') || die();

// We defined the web service functions to install.
// $functions = array(
//     'jsdemo_service_function_name_1' => array(
//             'classname'   => 'jsdemo_external',
//             'methodname'  => 'service_function_name_1',
//             'classpath'   => 'local/jsdemo/externallib.php',
//             'description' => 'Description about the function',
//             'type'        => 'read', // Type of operations (read, write).
//     ),
//     'jsdemo_service_function_name_2' => array(
//             'classname'   => 'jsdemo_external',
//             'methodname'  => 'service_function_name_2',
//             'classpath'   => 'local/jsdemo/externallib.php',
//             'description' => 'Description about the function',
//             'type'        => 'read', // Type of operations (read, write).
//     ),
// );

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
// $services = array(
//         'My service' => array(
//                 'functions' => array ('local_zsap_core_check_user_existence'),
//                 'restrictedusers' => 0,
//                 'enabled'=>1,
//         )
// );
/**
 * External Web Service Template
 *
 * @package    services
 * Developer: 2020 Ricoshae Pty Ltd (http://ricoshae.com.au)
 */

// We defined the web service functions to install.
$functions = array(
    'local_jsdemo_ajaxtest' => array(
        'classname' => 'local_jsdemo_external',
        'methodname' => 'ajaxtest',
        'classpath' => 'local/jsdemo/externallib.php',
        'description' => 'Return some data',
        'type' => 'read',
        'ajax' => true,
        'capabilities' => '',
        'loginrequired' => true
//                'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE)
    )
);