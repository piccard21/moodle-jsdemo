<?php

/**
 * @package  jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 * @doc https://docs.moodle.org/dev/Events_API
 */

// Event handlers (subscriptions) are defined here. It lists all the events that your plugin want to observe and be notified about.
 
defined('MOODLE_INTERNAL') || die();

