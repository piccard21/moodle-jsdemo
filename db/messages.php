<?php

/**
 * @package  jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 * @doc https://docs.moodle.org/dev/Message_API
 */

// Allow to declare your plugin as the messaging provider.
 
defined('MOODLE_INTERNAL') || die();

