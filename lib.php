<?php

/**
 * @package  jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 * @doc https://docs.moodle.org/dev/Plugin_files
 */
 
defined('MOODLE_INTERNAL') || die();

function local_jsdemo_extend_settings_navigation () { 
    global $PAGE;
    $PAGE->requires->js_call_amd('local_jsdemo/testme', 'init');
    $PAGE->requires->js_call_amd('local_jsdemo/ajaxtest', 'init');
    
}

function local_jsdemo_extend_navigation(global_navigation $nav) {
//    global $PAGE;
//    if ($PAGE->pagetype == 'site-index') {
//        // FOR AMD JAVASCRIPTS : https://docs.moodle.org/dev/Javascript_Modules
//        $PAGE->requires->js_call_amd('local_jsdemo/ishatar', 'init');
//        // JQuery plugin : https://docs.moodle.org/dev/jQuery_pre2.9
//        $PAGE->requires->jquery_plugin('jsdemo', 'local_jsdemo');
//    }
} 

function local_jsdemo_before_footer() {
//    global $PAGE;
//    $PAGE->requires->js_init_code("alert('before_footer');");
} 