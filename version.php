<?php

/**
 * @package  jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 * @doc https://docs.moodle.org/dev/version.php
 */

defined('MOODLE_INTERNAL') || die();

$plugin->component = 'local_jsdemo'; // Declare the type and name of this plugin.
$plugin->version = 2021010902; // Plugin released on 4th November 2017.
$plugin->requires  = 2020060900;  // Requires this Moodle version.
$plugin->maturity = MATURITY_ALPHA; // This is considered as ALPHA for production sites.
$plugin->release = 'v0.0.1'; // This is our first. 