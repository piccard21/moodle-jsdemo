import $ from 'jquery';
import Str from 'core/str';
import Ajax from 'core/ajax';

export const init = config => {
    window.console.log('config', config);
    window.console.log('jQuery', $);
    window.console.log('Str', Str);
    window.console.log('AJAX', Ajax);


    $(document).ready(function() {
        Ajax.call([{
            methodname: 'local_jsdemo_ajaxtest',
            args: {
                'id': 121
            },
        }])[0].done(function(response) {
            let data = JSON.parse(response);
            window.console.info("RESPONSE", data);
            return;
        }).fail(function(err) {
            window.console.log(err);
            // Notification.exception(new Error('Failed to load data'));
            return;
        });

    });


};