<?php

/**
 * jsdemo  web services implemntation.
 *
 * @package    jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 */

require_once($CFG->libdir."/externallib.php");
    
class local_jsdemo_external extends external_api{

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function ajaxtest_parameters()
    {
        return new external_function_parameters(
            array("id" => new external_value(PARAM_INT, "id"))
        );
    }

    public static function ajaxtest($id)
    {
        return json_encode(['aaa', 'bbb', 'ccc']);
    }


    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function ajaxtest_returns()
    {
        return new external_value(PARAM_RAW, 'The updated JSON output');
//        return new external_value();//new external_value(PARAM_TEXT, 'The welcome message + user first name');
    }
}