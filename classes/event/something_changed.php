<?php

/**
 * @package  jsdemo
 * @copyright 2021, Andreas Stephan <andreas.stephan@rz.ur.de>
 * @license MIT
 * @doc https://docs.moodle.org/dev/Event_2
 */

namespace jsdemo\event;

defined('MOODLE_INTERNAL') || die();


class something_changed extends base {

}
